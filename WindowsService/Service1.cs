﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService
{
    public partial class Service1 : ServiceBase
    {
        /// <summary>
        /// Gets or sets the path current log.
        /// </summary>
        /// <value>
        /// The path current log.
        /// </value>
        private string PathCurLog { get; set; }

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            this.OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                // Set default value
                PathCurLog = AppDomain.CurrentDomain.BaseDirectory + @"\Log_" + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".log";

                // Timer object
                timer1.Interval = 60 * 1000;  // 60000ms = 60s
                timer1.Enabled = true;
                timer1.Start();

                // File log
                SystemCore.Windows.Processing.WriteLog(PathCurLog, "Starting time", string.Empty);
            }
            catch (Exception er)
            {
                string errMsg = er.Message + (er.InnerException == null ? string.Empty : " " + er.InnerException.Message);
                SystemCore.Windows.Processing.WriteLog(PathCurLog, "Error when start", errMsg);
            }
        }

        protected override void OnStop()
        {
            try
            {
                // Set default value
                PathCurLog = AppDomain.CurrentDomain.BaseDirectory + @"\Log_" + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".log";

                // Timer object
                timer1.Stop();

                // Log
                SystemCore.Windows.Processing.WriteLog(PathCurLog, "Stop time", string.Empty);
            }
            catch (Exception er)
            {
                string errMsg = er.Message + (er.InnerException == null ? string.Empty : " " + er.InnerException.Message);
                SystemCore.Windows.Processing.WriteLog(PathCurLog, "Error when stop", errMsg);
            }
        }

        private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                // Log
                SystemCore.Windows.Processing.WriteLog(PathCurLog, "Running time", string.Empty);
            }
            catch (Exception er)
            {
                string errMsg = er.Message + (er.InnerException == null ? string.Empty : " " + er.InnerException.Message);
                SystemCore.Windows.Processing.WriteLog(PathCurLog, "Error when running", errMsg);
            }
        }
    }
}
