﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace HMC_Training_UI
{
    public static class Setting
    {
        // Define variable                
        /// <summary>
        /// Gets or sets a value indicating whether the is release status when 'Develop' = false, 'Build to live' = true.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is release; otherwise, <c>false</c>.
        /// </value>
        public static bool IsRelease { get; set; }
        /// <summary>
        /// Gets the name of the application configuration.
        /// </summary>
        /// <value>
        /// The name of the application configuration.
        /// </value>
        private static string AppConfigName { get; } = "Settings.config";

        #region  -- Get/Set Config
        public static string Realm { get { return GetConfigValue("Realm", ""); } set { SaveConfigValue("Realm", value); } }
        public static string URL { get { return GetConfigValue("URL", ""); } set { SaveConfigValue("URL", value); } }
        public static string ConsKey { get { return GetConfigValue("ConsKey", ""); } set { SaveConfigValue("ConsKey", value); } }
        #endregion

        private static string GetConfigValue(string key, string defaultValue = null, int length = 0)
        {
            // Get path
            string appWorkingPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string projDirectoryPath = Directory.GetParent(appWorkingPath).Parent.FullName;

            // Get .config file
            string configFile = Path.Combine((IsRelease == false ? projDirectoryPath : appWorkingPath), AppConfigName);

            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFile;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

            if (config.AppSettings.Settings[key] != null)
            {
                if (length > 0)
                {
                    return config.AppSettings.Settings[key].Value.ToString().PadRight(length).Substring(0, length).Trim();
                }
                else
                {
                    return config.AppSettings.Settings[key].Value.ToString().Trim();
                }
            }
            else
            {
                return defaultValue ?? string.Empty;
            }
        }

        public static void SaveConfigValue(string key, string value)
        {
            // Get path
            string appWorkingPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string projDirectoryPath = Directory.GetParent(appWorkingPath).Parent.FullName;

            // Get .config file
            string configFile = Path.Combine((IsRelease == false ? projDirectoryPath : appWorkingPath), AppConfigName);

            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFile;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

            config.AppSettings.Settings[key].Value = value;
            config.Save();
        }
    }
}
