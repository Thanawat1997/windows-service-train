﻿namespace HMC_Training_UI
{
    partial class frm_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Settings));
            this.btn_Save = new System.Windows.Forms.Button();
            this.tb_Realm = new System.Windows.Forms.TextBox();
            this.lbl_Realm = new System.Windows.Forms.Label();
            this.lbl_URL = new System.Windows.Forms.Label();
            this.tb_URL = new System.Windows.Forms.TextBox();
            this.lbl_ConsKey = new System.Windows.Forms.Label();
            this.tb_ConsKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gb_Services = new System.Windows.Forms.GroupBox();
            this.lbl_ServStatus = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gb_Services.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.Aquamarine;
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Save.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.Location = new System.Drawing.Point(224, 233);
            this.btn_Save.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(64, 27);
            this.btn_Save.TabIndex = 0;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = false;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // tb_Realm
            // 
            this.tb_Realm.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Realm.Location = new System.Drawing.Point(90, 21);
            this.tb_Realm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tb_Realm.Name = "tb_Realm";
            this.tb_Realm.Size = new System.Drawing.Size(198, 25);
            this.tb_Realm.TabIndex = 1;
            // 
            // lbl_Realm
            // 
            this.lbl_Realm.AutoSize = true;
            this.lbl_Realm.Location = new System.Drawing.Point(31, 24);
            this.lbl_Realm.Name = "lbl_Realm";
            this.lbl_Realm.Size = new System.Drawing.Size(44, 17);
            this.lbl_Realm.TabIndex = 3;
            this.lbl_Realm.Text = "Realm";
            // 
            // lbl_URL
            // 
            this.lbl_URL.AutoSize = true;
            this.lbl_URL.Location = new System.Drawing.Point(31, 57);
            this.lbl_URL.Name = "lbl_URL";
            this.lbl_URL.Size = new System.Drawing.Size(31, 17);
            this.lbl_URL.TabIndex = 5;
            this.lbl_URL.Text = "URL";
            // 
            // tb_URL
            // 
            this.tb_URL.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_URL.Location = new System.Drawing.Point(90, 54);
            this.tb_URL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tb_URL.Name = "tb_URL";
            this.tb_URL.Size = new System.Drawing.Size(198, 25);
            this.tb_URL.TabIndex = 4;
            // 
            // lbl_ConsKey
            // 
            this.lbl_ConsKey.AutoSize = true;
            this.lbl_ConsKey.Location = new System.Drawing.Point(31, 90);
            this.lbl_ConsKey.Name = "lbl_ConsKey";
            this.lbl_ConsKey.Size = new System.Drawing.Size(92, 17);
            this.lbl_ConsKey.TabIndex = 7;
            this.lbl_ConsKey.Text = "Consumer Key";
            // 
            // tb_ConsKey
            // 
            this.tb_ConsKey.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ConsKey.Location = new System.Drawing.Point(34, 115);
            this.tb_ConsKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tb_ConsKey.Name = "tb_ConsKey";
            this.tb_ConsKey.Size = new System.Drawing.Size(254, 25);
            this.tb_ConsKey.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Leelawadee UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "Example: XXXXXX";
            // 
            // gb_Services
            // 
            this.gb_Services.Controls.Add(this.lbl_ServStatus);
            this.gb_Services.Location = new System.Drawing.Point(33, 161);
            this.gb_Services.Name = "gb_Services";
            this.gb_Services.Size = new System.Drawing.Size(255, 65);
            this.gb_Services.TabIndex = 9;
            this.gb_Services.TabStop = false;
            this.gb_Services.Text = "Service Status";
            // 
            // lbl_ServStatus
            // 
            this.lbl_ServStatus.AutoSize = true;
            this.lbl_ServStatus.Font = new System.Drawing.Font("Leelawadee UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ServStatus.Location = new System.Drawing.Point(92, 21);
            this.lbl_ServStatus.Name = "lbl_ServStatus";
            this.lbl_ServStatus.Size = new System.Drawing.Size(51, 30);
            this.lbl_ServStatus.TabIndex = 0;
            this.lbl_ServStatus.Text = "N/A";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frm_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 273);
            this.Controls.Add(this.gb_Services);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_ConsKey);
            this.Controls.Add(this.tb_ConsKey);
            this.Controls.Add(this.lbl_URL);
            this.Controls.Add(this.tb_URL);
            this.Controls.Add(this.lbl_Realm);
            this.Controls.Add(this.tb_Realm);
            this.Controls.Add(this.btn_Save);
            this.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.frm_Settings_Load);
            this.gb_Services.ResumeLayout(false);
            this.gb_Services.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TextBox tb_Realm;
        private System.Windows.Forms.Label lbl_Realm;
        private System.Windows.Forms.Label lbl_URL;
        private System.Windows.Forms.TextBox tb_URL;
        private System.Windows.Forms.Label lbl_ConsKey;
        private System.Windows.Forms.TextBox tb_ConsKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gb_Services;
        private System.Windows.Forms.Label lbl_ServStatus;
        private System.Windows.Forms.Timer timer1;
    }
}

