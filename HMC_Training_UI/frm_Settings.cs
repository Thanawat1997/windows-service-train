﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMC_Training_UI
{
    public partial class frm_Settings : Form
    {
        public frm_Settings()
        {
            InitializeComponent();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            // Respone to user
            var isYes = MessageBox.Show("Do you confirm to save information?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (isYes == DialogResult.Yes)
            {
                // Set value into config file
                Setting.Realm = tb_Realm.Text;
                Setting.URL = tb_URL.Text;
                Setting.ConsKey = tb_ConsKey.Text;
                MessageBox.Show("Save information, Sucessfully", this.Text, MessageBoxButtons.OK);
            }
        }

        private void frm_Settings_Load(object sender, EventArgs e)
        {
            // Call function
            Get_ServiceStatus("Service1");

            // Get value from config file
            tb_Realm.Text = Setting.Realm;
            tb_URL.Text = Setting.URL;
            tb_ConsKey.Text = Setting.ConsKey;

            // Set default timer object
            timer1.Enabled = true;
            timer1.Interval = 5000;

        }

        private void Get_ServiceStatus(string servicename)
        {
            Console.WriteLine("Working....");

            // Get all service in the machine
            System.ServiceProcess.ServiceController[] oServ = System.ServiceProcess.ServiceController.GetServices(Environment.MachineName);
            // Find the service that you want
            var oServ_Mine = oServ.FirstOrDefault(o => o.ServiceName == servicename);

            // Check service object
            if (oServ_Mine != null)
            {
                oServ_Mine.Refresh();
                // Check service status
                if (oServ_Mine.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                    lbl_ServStatus.Text = "Running na krub";
                else if (oServ_Mine.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    lbl_ServStatus.Text = "Stopped na krub";
                else
                    lbl_ServStatus.Text = "N/A";
            }
            else
                lbl_ServStatus.Text = "N/A";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Get_ServiceStatus("Service1");
        }
    }
}
